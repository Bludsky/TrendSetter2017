A simple Twitter bot in python

# Usage
0. Twitter wrapper required http://mike.verdone.ca/twitter/
1. Create a new twitter app for your twitter account (mobile phone required for authorization)
2. Generate access token and secret
3. Set up your configuration in configuration.py
4. Run the bot

# Functionality
* The bot likes top x tweets of user's timeline
* The bot can post status on its home timeline