# Access token section
ACCESS_TOKEN = "INSERT_HERE"
ACCESS_TOKEN_SECRET = "INSERT_HERE"

# Consumer section
CONSUMER_KEY = "INSERT_HERE"
CONSUMER_SECRET = "INSERT_HERE"

# Bot behaviour section

# Run all actions each x seconds
REPEAT_SECONDS = 60.0

# Screen name of user whose tweets will be liked
LIKE_SCREEN_NAME = "QAS2Team"
# Amount of tweets from user's timeline the bot can like in one go (max 200)
LIKE_TWEET_COUNT = 20
