from twitter import *
from config import configuration
import time

# TrendSetter2017
# Setup your configuration first!
# Keep the toluene flowing
# Implement new functionality in compliance with Twitter API https://dev.twitter.com/rest/reference
class TrendSetter:
    def __init__(self):
        self.t = Twitter(
            auth=OAuth(configuration.ACCESS_TOKEN, configuration.ACCESS_TOKEN_SECRET, configuration.CONSUMER_KEY,
                       configuration.CONSUMER_SECRET))

    # Post status on your home timeline
    def postStatus(self, status):
        self.t.statuses.update(status=status)

    # Reads user's timeline and like their stuff
    def likeTimeline(self, handle):
        for tweet in self.t.statuses.user_timeline(screen_name=handle, count=configuration.LIKE_TWEET_COUNT):
            tweet_id = tweet['id']
            try:
                print "Liking tweet with id %d" % tweet_id
                self.t.favorites.create(_id=tweet_id)
            except TwitterHTTPError:
                print "Could not like this tweet (probably already liked)"

    # Run bot that periodically does its stuff
    def runBot(self):
        print "#TrendSetter2017 is running..."
        start_time = time.time()
        while True:
            print "Liking %s's timeline" % configuration.LIKE_SCREEN_NAME
            self.likeTimeline(configuration.LIKE_SCREEN_NAME)
            print "All jobs done, resting for a bit..."
            time.sleep(configuration.REPEAT_SECONDS - ((time.time() - start_time) % configuration.REPEAT_SECONDS))

# Run the bot
if __name__ == "__main__":
    tr = TrendSetter()
    tr.runBot()